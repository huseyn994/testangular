import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { TeamsComponent } from './teams/teams.component';
import { TeamService } from './team.service';
import { MemberComponent } from './member/member.component';


@NgModule({
  declarations: [
    AppComponent,
    TeamsComponent,
    MemberComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [TeamService],
  bootstrap: [AppComponent]
})
export class AppModule { }