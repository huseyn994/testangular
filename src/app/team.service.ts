import { Injectable } from '@angular/core';
import { Teams } from './teams';
import { TEAMS } from './mock-teams';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class TeamService {
  constructor(private http: HttpClient) { }
  
  private teamUrl = "https://api.jsonbin.io/b/5a5ba126e78158547788a309"
  
  getTeams ():Observable<Teams[]>  {
   
    return this.http.get<Teams[]>(this.teamUrl)
    .pipe(
      tap(teams => console.log(teams))
    );
  }
}
